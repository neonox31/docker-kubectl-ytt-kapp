FROM alpine AS base

# Build
FROM base AS build

ARG KUBECTL_VERSION=""
ARG YTT_VERSION=""
ARG KAPP_VERSION=""

WORKDIR /binaries

RUN apk --no-cache add curl

RUN if [ -z "${KUBECTL_VERSION}" ]; then KUBECTL_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt); fi && \
    echo "Downloading kubectl ${KUBECTL_VERSION}..." && \
    curl -sLo kubectl "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" && \
    chmod +x kubectl

RUN if [ -z "${YTT_VERSION}" ]; then YTT_VERSION=$(curl -s "https://api.github.com/repos/carvel-dev/ytt/releases/latest" | sed -n 's/.*tag_name":\s"\(.*\)".*/\1/p' | head -1); fi && \
    echo "Downloading ytt ${YTT_VERSION}..." && \
    curl -sLo ytt "https://github.com/carvel-dev/ytt/releases/download/${YTT_VERSION}/ytt-linux-amd64" && \
    chmod +x ytt

RUN if [ -z "${KAPP_VERSION}" ]; then KAPP_VERSION=$(curl -s "https://api.github.com/repos/carvel-dev/kapp/releases/latest" | sed -n 's/.*tag_name":\s"\(.*\)".*/\1/p' | head -1); fi && \
    echo "Downloading kapp ${KAPP_VERSION}..." && \
    curl -sLo kapp "https://github.com/carvel-dev/kapp/releases/download/${KAPP_VERSION}/kapp-linux-amd64" && \
    chmod +x kapp

# Runtime
FROM base AS runtime

RUN apk --no-cache add curl

COPY --from=build /binaries/kubectl /usr/local/bin/kubectl
COPY --from=build /binaries/ytt /usr/local/bin/ytt
COPY --from=build /binaries/kapp /usr/local/bin/kapp
